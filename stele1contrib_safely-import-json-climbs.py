#!/usr/bin/env python3

import stele1.climb
import stele1.filesystem
import uuid
import json
import sys
import tempfile
import os
import subprocess

def cli_help(cmd):
    print(f'usage {cmd} PROJECT_DIR FILE_WITH_JSON_ARRAY_OF_CLIMBS');

#
# this is already covered by ``intersection_of_all_names''
#
def shares_name(climb, newclimb):
    if 'name' in newclimb:
        return climb.get_name().to_data().lower() == newclimb['name'].lower()
    else:
        return False

def intersection_of_all_names(climb, newclimb):
    oldnames = set((climb.get_name().to_data().lower(),)) if climb.get_name() else set()
    if climb.get_alternate_names():
        for n in climb.get_alternate_names().to_data():
            oldnames.add(n.lower())
    newnames = set((newclimb['name'].lower(),)) if ('name' in newclimb) else set()
    if 'alternateNames' in newclimb:
        for n in newclimb['alternateNames']:
            newnames.add(n.lower())
    return oldnames.intersection(newnames)

def shares_uuid(climb, newclimb):
    if 'uuid' in newclimb:
        return climb.get_uuid().to_data().lower() == newclimb['uuid'].lower()

#  only tests "_id" suffix
#
#     "fields": {
#         "added_iso_timestamp": "2022-06-10T01:58:18-07:00",
#         "mountainproject_id": "ABCDE"
#     },
#     "fields": {
#         "mountainproject_id": "abcde"
#     },
#
# returns
#
#    {
#      'mountainproject_id': ['ABCDE', 'abcde'],
#    }
#
def shared_id_fields(climb, newclimb):
    climbfields = climb.get_fields().to_data() if climb.get_fields() else {}
    common_id_fields = {}
    if 'fields' in newclimb:
        for k, v in newclimb['fields'].items():
            if k.endswith('_id') and (k in climbfields):
                if v.lower() == climbfields[k].lower():
                    common_id_fields[k] = [climbfields[k], v]
    return common_id_fields

def list_potential_matching_climbs(proj, newclimb):
    conflicts = {}
    for uuid in proj.climb_uuids():
        similarities = []
        climb = proj.get_climb_by_uuid(uuid) 
        if shares_uuid(climb, newclimb):
            similarities.append('matching uuid')
        for n in intersection_of_all_names(climb, newclimb):
            similarities.append(f'shares a name {n}')
        for field in shared_id_fields(climb, newclimb).keys():
            similarities.append(f'shares a value in fields.{field}')
        if similarities:
            conflicts[uuid.to_data()] = {
                "source_climb": newclimb,
                "climb": climb.to_data(),
                "similarities": similarities,
            }
    return conflicts

def open_json_editor(txt):
    EDITOR = os.environ.get('EDITOR','vim')
    with tempfile.NamedTemporaryFile(suffix=".tmp.json", mode="w+") as tf:
        header_text = [
            ' edit this file into an appropriate Climb JSON',
            ''
            ' "" will quit (an empty file)',
            ' "q" will quit',
            ' "r" will reset to the contents',
            ''
        ]
        header_lines = [f'# {line}\n' for line in header_text]
        tf.writelines(header_lines)
        tf.write(txt)
        tf.flush()
        while True:
            subprocess.call([EDITOR, tf.name])
            tf.seek(0)
            content = "\n".join([line for line in tf.read().split('\n') if not line.startswith('#')]).strip()
            if not content:      return None
            elif content == 'q': return None
            elif content == 'r':
                tf.seek(0)
                tf.truncate()
                tf.writelines(header_lines)
                tf.write(txt)
                tf.flush()
            else:
                try:
                    j = json.dumps(json.loads(content), indent=2)
                    return j
                except json.decoder.JSONDecodeError as err:
                    tf.seek(0)
                    tf.truncate()
                    tf.writelines(header_lines)
                    tf.write(f'# JSON was malformed: {str(err)}\n')
                    tf.write(content)
                    tf.flush()

def starting_text(newclimb, matches):
    txt = [
        '#',
        '#     This will CREATE OR REPLACE a climb based on the uuid',
        '#',
        '#     you climb will _need_ a uuid',
        '#     without one, it will fail',
        '#     if you don\'t have one, you can use this',
        f'#     "uuid": "{str(uuid.uuid4())}",',
        '# ',
        '#     here is the candinate:',
        '# ',
        json.dumps(newclimb, indent=2),
    ]
    if matches:
        txt.append('')
        for id, match in matches.items():
            txt.append('#')
            txt.append(f'# climb {id} matches because it...')
            for s in match['similarities']:
                txt.append(f'#     - {s}')
            txt.append(json.dumps(match['climb'], indent=2))
    else:
        txt.append('# no similar climbs were found in the project')
    return '\n'.join(txt)

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 3:
        cli_help(sys.argv[0])
        sys.exit('expected 2 arguments')
    proj = stele1.filesystem.Project(sys.argv[1]);
    with open(sys.argv[2], 'r') as f:
        for climb in json.load(f):
            matches = list_potential_matching_climbs(proj, climb)
            if matches:
                j = open_json_editor(starting_text(climb, matches))
                if j:
                    proj.set_climb(stele1.climb.Climb.from_data(json.loads(j)))
                else:
                    print(f'not adding {climb["name"]}')
            else:
                climb['uuid'] = str(uuid.uuid4())
                proj.set_climb(stele1.climb.Climb.from_data(climb))
